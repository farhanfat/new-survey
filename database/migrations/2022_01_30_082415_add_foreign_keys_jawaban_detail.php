<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysJawabanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawaban_detail', function (Blueprint $table) {
            $table->foreign('id_jawaban','fk_detail_jawabandetail_to_jawaban')->references('id')->on('jawaban')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('pertanyaan_id','fk_detail_jawabandetail_to_pertanyaan')->references('id')->on('pertanyaan')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawaban_detail', function (Blueprint $table) {
            $table->dropForeign('fk_detail_jawabandetail_to_jawaban');
            $table->dropForeign('fk_detail_jawabandetail_to_pertanyaan');
        });
    }
}
