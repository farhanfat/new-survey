<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Support\Facades\Log;
use KejaksaanDev\PortalLogger\Http\Traits\WriterLogger;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public  function  report(Throwable  $e)
	{
		try {
			$statusCode = 500;
			$className = get_class($e);

			if ($e instanceof BadRequestHttpException) {
				$statusCode = 400;
			} else  if ($e instanceof MethodNotAllowedHttpException) {
				$statusCode = 405;
			} else  if ($e instanceof NotFoundHttpException) {
				$statusCode = 404;
			} else  if ($e instanceof UnauthorizedHttpException) {
				$statusCode = 401;
			}

			WriterLogger::writeLog(request(), (string) $statusCode, 'error', null, $className, $e->getMessage());
		}
		catch (\Exception  $ex) {
			Log::error("Failed log exception handler cause : {$ex->getMessage()}");
		}
	}

}
