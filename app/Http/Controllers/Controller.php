<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function index()
    // {
    //     $users = DB::table('users as s1')
    //     ->join('satkers as s2','s2.id','=','s1.parent_satker')
    //     ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
    //     ->get();
    //     return view('pages.admin.dashboard', compact('datasatker'));
    // }
}
