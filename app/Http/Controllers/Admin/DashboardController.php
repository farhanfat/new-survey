<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Satkers;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Jawaban;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {

        if(Auth::user()->nip != null){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=".env('USER_MYSIMKARI')."&password=".env('PASS_MYSIMKARI'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if($responCode == 200 && Auth::user()->nip != null){
                $data = json_decode($response);    
                $token = $data->access_token;
                $headers = [
                    'Authorization: Bearer ' .$token,
                ];
                $url = 'https://api-dev.kejaksaan.go.id/mysimkariv2/api/v2/web/pegawai?nip='.Auth::user()->nip;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                // curl_setopt($ch, CURLOPT_POSTFIELDS,$post); 
                try {
                    $response = curl_exec($ch);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($ch);
                $result = json_decode($response);
                $kode_satker = $result->data->kode_satker;
                $satker = Satkers::where('kode_satker',$kode_satker)->select('id')->first();
                $id_satker = $satker->id;
                $user = User::where('nip', Auth::user()->nip)
                ->update([
                    'satker_id' => $id_satker
                ]);
            }
        }

        

        $total_user = DB::select("
            select count(name) as total_user from users
        ");

        $total_survey = DB::select("
            select count(jawaban_dari) as total_survey from jawaban
        ");

        $total_survey_today = Jawaban::where('created_at', '>=', Carbon::today())->count();

        $jumlah_survey = DB::select("
        select s.nama_satker as nama_satker, count(j.satker_id) as jumlah from satkers s join jawaban j on s.id = j.satker_id
        group by  s.nama_satker, j.satker_id
        order by count(j.satker_id) desc
        limit 10
        ");

        return view('pages.admin.dashboard',compact('total_user','total_survey','total_survey_today','jumlah_survey'));
    }
}
