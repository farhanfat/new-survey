<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Satkers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $user = DB::table('users as u')
        ->join('satkers as s','s.id','=','u.satker_id')
        ->select('u.id as id','u.username as username','u.name as name', 'u.email as email', 'u.password as password','u.roles as roles','s.nama_satker')
        ->get();
        // $user = DB::table('users')->where('roles', 2);
        return view('pages.admin.pengguna.index', compact('user'));
    }

    public function show($id)
    {   
        // $user = User::find($id);

        $user = DB::table('users as u')
        ->join('satkers as s','s.id','=','u.satker_id')
        ->select('u.id as id','u.username as username','u.name as name', 'u.email as email', 'u.password as password','u.roles as roles','s.nama_satker')
        ->where('u.id',$id)
        ->first();

        return view('pages.admin.pengguna.show', compact('user'));
    }

    public function edit($id)
    {
        $satker = Satkers::all();
        $user = DB::table('users as u')
        ->join('satkers as s','s.id','=','u.satker_id')
        ->select('u.id as id','u.username as username','u.name as name', 'u.email as email', 'u.password as password','u.roles as roles','s.nama_satker','s.id as id_satker')
        ->where('u.id',$id)
        ->first();

        return view('pages.admin.pengguna.edit', compact('user','satker'));
    }

    public function create()
    {
        $user = User::all();
        $satker = Satkers::all();
        return view('pages.admin.pengguna.create', compact('user','satker'));
    }

    public function store(Request $request)
    {
        

        $this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
            'roles' => 'required',
            'password' => 'required',
            'satker_id' => 'required',
            'username' => 'required'
    	]);

        $query = DB::table('users')->insert([
            "name" => $request["name"],
            "email" => $request["email"],
            "roles" => $request["roles"],
            "password" => Hash::make($request->password),
            "satker_id" => $request["satker_id"],
            "username" => $request["username"],

        ]);
 
        toast()->success('Berhasil Menyimpan Data');
    	return redirect('/admin/pengguna');
    }

    public function update(Request $request, $id)
    {
        // $this->validate($request,[
    	// 	'urutan' => 'required'
    	// ]);
        if ($request["password"] != "") {
            User::where('id', $id)
            ->update([
                "password" => Hash::make($request->password),
            ]);
        };

        User::where('id', $id)
            ->update([
                "name" => $request["name"],
                "email" => $request["email"],
                "roles" => $request["roles"],
                "satker_id" => $request["satker_id"],
                "username" => $request["username"],
            ]);


        

        toast()->success('Berhasil Mengubah Data');
    	return redirect('/admin/pengguna');
    }

    public function destroy($id)
    {
        {
            $user = User::find($id);
            $user->delete();
            toast()->success('Berhasil Menghapus Data');
            return redirect('/admin/pengguna');
        }
    }
}
