<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OpenSurvey;
use App\Models\OpenSurveyBlok;
use App\Models\OpenSurveyBlokPertanyaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Http\Response;

class OpensurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = OpenSurvey::all();   

        return view('pages.admin.opensurvey.index', compact('data'));
    }

    public function show($id)
    {   

        $data = OpenSurvey::all()->where('id', $id)->first();

        return view('pages.admin.opensurvey.show', compact('data'));
    }

    public function edit($id)
    {
        $data = OpenSurvey::all()->where('id', $id)->first();

        return view('pages.admin.opensurvey.edit', compact('data'));
    }

    public function create()
    {

        return view('pages.admin.opensurvey.create');
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $service = OpenSurvey::create($data);
 
        toast()->success('Berhasil Menyimpan Data');
    	return redirect('/admin/opensurvey');
    }

    public function update(Request $request, $id)
    {
        OpenSurvey::where('id', $id)
            ->update([
                'judul_survey' => $request["judul_survey"],
                'deskripsi_survey' => $request["deskripsi_survey"],
                'survey_link' => $request["survey_link"],
                'periode_start' => $request["periode_start"],
                'periode_end' => $request["periode_end"],
                'is_open' => $request["is_open"],
                'status' => $request["status"],
            ]);

        toast()->success('Berhasil Mengubah Data');
    	return redirect('/admin/opensurvey');
    }

    public function destroy($id)
    {
        $data = OpenSurvey::find($id);
        $data->delete();
        toast()->success('Berhasil Menghapus Data');
        return redirect('/admin/opensurvey');
    }

    public function detail($id)
    {   

        $data = OpenSurvey::all()->where('id', $id)->first();

        $blok = DB::table('os_survey_blok')
        ->select('id','id_survey','nama_blok','deskripsi_blok','urutan','slug_blok')
        ->where('id_survey', $id)
        ->orderBy('urutan','asc')
        ->get();

        $bloks = DB::table('os_survey_blok')
        ->select('id','id_survey','nama_blok','deskripsi_blok','urutan','slug_blok')
        ->where('id_survey', $id)
        ->orderBy('urutan','asc')
        ->get();

        $blokz = DB::table('os_survey_blok')
        ->select('id','id_survey','nama_blok','deskripsi_blok','urutan','slug_blok')
        ->where('id_survey', $id)
        ->orderBy('urutan','asc')
        ->get();

        $pertanyaan = OpenSurveyBlokPertanyaan::orderBy('id','desc')->paginate(10);

        $count_pertanyaan = OpenSurveyBlok::where('id_survey', $id)->groupBy('id_survey')->count();

        return view('pages.admin.opensurvey.detail', compact('data','blok', 'bloks','blokz','pertanyaan','count_pertanyaan'));
    }


    public function simpanblok(Request $request)
    {   
        $nama_bloks = str_replace(" ","-",$request["nama_blok"]);
        $slug_blok = $request["survey_link"].'-'.$nama_bloks;

        OpenSurveyBlok::create([
            "id_survey" => $request["id_survey"],
            "nama_blok" => $request["nama_blok"],
            "deskripsi_blok" => $request["deskripsi_blok"],
            "urutan" => $request["urutan"],
            "slug_blok" => $slug_blok,
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);

        // return $this->detail($request["id_survey"]);
        toast()->success('Berhasil Menyimpan Data Blok');
        return redirect('/admin/opensurvey/detail/'.$request['id_survey']);
    }

    public function ubahblok(Request $request, $id)
    {   

        OpenSurveyBlok::where('id', $id)
            ->update([
                "nama_blok" => $request["nama_blok"],
                "deskripsi_blok" => $request["deskripsi_blok"],
                "urutan" => $request["urutan"],
                'updated_at'        => Carbon::now()
            ]);

        toast()->success('Berhasil Mengubah Data Blok');
        return redirect('/admin/opensurvey/detail/'.$request['id_survey']);
    }

    public function hapusblok(Request $request, $id)
    {   

        $data = OpenSurveyBlok::find($id);
        $data->delete();

        toast()->success('Berhasil Menghapus Data Blok');
        return redirect('/admin/opensurvey/detail/'.$request['id_survey']);
    }

    public function api_pertanyaan($id_survey_blok)
    {
        $get_pertanyaan = OpenSurveyBlokPertanyaan::where('id_survey_blok', $id_survey_blok)->orderByRaw('urutan ASC')->get();

        return $get_pertanyaan;
    }

    public function get_pertanyaan($id_survey_blok)
    {
        $get_pertanyaan = OpenSurveyBlokPertanyaan::where('id_survey_blok', $id_survey_blok)->orderBy('urutan','asc')->get();

        return response()->json($get_pertanyaan, 200);
    }

    public function simpanpertanyaan(Request $request)
    {   

        foreach($request->input('pertanyaan') as $key => $value) {
            OpenSurveyBlokPertanyaan::create([
                'pertanyaan'=>$value,
                'id_survey_blok'=>$request['id_survey_blok']
            ]);
        }

        // return $this->detail($request["id_survey"]);
        toast()->success('Berhasil Menyimpan Data Blok');
        // return redirect('/admin/opensurvey/detail/'.$request['id_survey']);
        return back();
    }

     public function storepertanyaan(Request $request)
     {
         $request->validate([
           'pertanyaan'       => 'required|max:255',
           'urutan' => 'required',
         ]);

         $pertanyaan = OpenSurveyBlokPertanyaan::updateOrCreate(['id' => $request->id], [
                   'pertanyaan' => $request->pertanyaan,
                   'urutan' => $request->urutan
                 ]);

         return response()->json(['code'=>200, 'message'=>'Created successfully','data' => $pertanyaan], 200);

     }

    public function editpertanyaan($id)
    {
        $pertanyaan = OpenSurveyBlokPertanyaan::find($id);

        return Response::json($pertanyaan);
    }


    public function showpertanyaan($id)
    {
        $pertanyaan = OpenSurveyBlokPertanyaan::find($id);

        return response()->json($pertanyaan);
    }

    public function destroypertanyaan($id)
    {
        $pertanyaan= OpenSurveyBlokPertanyaan::find($id)->delete();

        return Response::json($pertanyaan);
    }
}
