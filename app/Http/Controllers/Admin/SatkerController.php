<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Satkers;
use Illuminate\Support\Facades\DB;

class SatkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datasatker = DB::table('satkers as s1')
        ->join('satkers as s2','s2.id','=','s1.parent_satker')
        ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        ->get();
        return view('pages.admin.satker.index', compact('datasatker'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datasatker = Satkers::all();
        return view('pages.admin.satker.create', compact('datasatker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $this->validate($request,[
    		'nama_satker' => 'required',
    		'parent_satker' => 'required',
            'tipe_satker' => 'required',
    	]);
 
        $data = $request->all();
        $service = Satkers::create($data);
 
        toast()->success('Berhasil Menyimpan Data');
    	return redirect('/admin/satker');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datasatker = DB::table('satkers as s1')
        ->join('satkers as s2','s2.id','=','s1.parent_satker')
        ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        ->where('s1.id', $id)
        ->first();

        return view('pages.admin.satker.show', compact('datasatker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datasatker = DB::table('satkers as s1')
        ->join('satkers as s2','s2.id','=','s1.parent_satker')
        ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.id as id_parent_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        ->where('s1.id', $id)
        ->first();

        $datasatker2 = DB::table('satkers as s1')
        ->join('satkers as s2','s2.id','=','s1.parent_satker')
        ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.id as id_parent_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        ->get();

        return view('pages.admin.satker.edit', compact('datasatker','datasatker2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Satkers::where('id', $id)
            ->update([
                'nama_satker' => $request["nama_satker"],
                'parent_satker' => $request["parent_satker"],
                'tipe_satker' => $request["tipe_satker"],
            ]);

        toast()->success('Berhasil Mengubah Data');
    	return redirect('/admin/satker');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            $datasatker = Satkers::find($id);
            $datasatker->delete();
            toast()->success('Berhasil Menghapus Data');
            return redirect('/admin/satker');
        }
    }
}
