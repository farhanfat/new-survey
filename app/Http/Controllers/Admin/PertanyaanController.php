<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pertanyaans;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    //
    

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {   //$datapertanyaan = DB::table('pertanyaans')->get();
         $datapertanyaan = Pertanyaans::all();    
        // $datasatker = DB::table('satkers as s1')
        // ->join('satkers as s2','s2.id','=','s1.parent_satker')
        // ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        // ->get();
        return view('pages.admin.pertanyaan.index', compact('datapertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('pages.admin.pertanyaan.create');
        $datapertanyaan = Pertanyaans::all();
        return view('pages.admin.pertanyaan.create', compact('datapertanyaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $this->validate($request,[
    		'urutan' => 'required',
    		'pertanyaan' => 'required'
    	]);
 
        $data = $request->all();
        $service = Pertanyaans::create($data);
 
        toast()->success('Berhasil Menyimpan Data');
    	return redirect('/admin/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        // $datapertanyaan = Pertanyaans::where($id)->first();
        $datapertanyaan = Pertanyaans::find($id);
        // $datasatker = DB::table('satkers as s1')
        // ->join('satkers as s2','s2.id','=','s1.parent_satker')
        // ->select('s1.id as id','s1.nama_satker as nama_satker', 's2.nama_satker as parent_satker', 's1.tipe_satker as tipe_satker')
        // ->where('s1.id', $id)
        // ->first();

        return view('pages.admin.pertanyaan.show', compact('datapertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $datapertanyaan = Pertanyaans::find($id);
        return view('pages.admin.pertanyaan.edit', compact('datapertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'urutan' => 'required'
    	]);
        Pertanyaans::where('id', $id)
            ->update([
                'urutan' => $request["urutan"],
                'pertanyaan' => $request["pertanyaan"],
            ]);

        toast()->success('Berhasil Mengubah Data');
    	return redirect('/admin/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {
            $datapertanyaan = Pertanyaans::find($id);
            $datapertanyaan->delete();
            toast()->success('Berhasil Menghapus Data');
            return redirect('/admin/pertanyaan');
        }
    }
}
