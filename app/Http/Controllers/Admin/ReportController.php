<?php

namespace App\Http\Controllers\Admin;

use App\Exports\LaporanHasilSurveyExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Satkers;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function hasilsurvey()
    {
        // $query = DB::table('jawaban as j')
        // ->join('jawaban_detail as jd','j.id','=','jd.id_jawaban')
        // ->join('satkers as s', 's.id','=','j.satker_id')
        // ->select('j.nama_satker as nama_satker','count(jd.jawaban->where(jd.jawaban))')
        // ->get();


        $query = DB::select("
        select s.nama_satker, s.id,
        COUNT(case when jd.jawaban = 'sangat_kurang' then 1 else null end) as sangat_kurang,
        COUNT(case when jd.jawaban = 'kurang' then 1 else null end) as kurang,
        COUNT(case when jd.jawaban = 'cukup' then 1 else null end) as cukup,
        COUNT(case when jd.jawaban = 'baik' then 1 else null end) as baik,
        COUNT(case when jd.jawaban = 'sangat_baik' then 1 else null end) as sangat_baik
        FROM
        jawaban as j 
        JOIN
        jawaban_detail as jd on j.id = jd.id_jawaban
        RIGHT JOIN
        satkers as s on s.id = j.satker_id
        GROUP BY s.nama_satker, s.id
        order by s.id asc 
        ");

        return view('pages.admin.report.hasilsurvey',compact('query'));
    }

    public function exportHasilSurvey(){
        $date = \Carbon\Carbon::now();
        $fileName = $date.'-Laporan-Hasil-Survey.xlsx';

        return Excel::download(new LaporanHasilSurveyExport(), $fileName);
    }

    public function hasilSurveySatker(){
        $data = Satkers::all();
        return view('pages.admin.report.hasilsurveysatker',compact('data'));
    }

    public function hasilSurveySatkerDetail(Request $request){

        $id_satker = $request->id_satker;

        $query = DB::select("
        select s.id as id, s.nama_satker as nama_satker, p.pertanyaan as pertanyaan,
        COUNT(case when jd.jawaban = 'sangat_kurang' then 1 else null end) as sangat_kurang,
        COUNT(case when jd.jawaban = 'kurang' then 1 else null end) as kurang,
        COUNT(case when jd.jawaban = 'cukup' then 1 else null end) as cukup,
        COUNT(case when jd.jawaban = 'baik' then 1 else null end) as baik,
        COUNT(case when jd.jawaban = 'sangat_baik' then 1 else null end) as sangat_baik
        from satkers s
        left join jawaban j on s.id = j.satker_id
        left join jawaban_detail jd on j.id = jd.id_jawaban
        left join pertanyaans p on jd.pertanyaan_id = p.id
        where s.id = $id_satker
        GROUP BY s.id, s.nama_satker, p.pertanyaan

        ");

        $query2 = DB::select("
        select s.nama_satker as nama_satker, s.id as id,
        COUNT(case when jd.jawaban = 'sangat_kurang' then 1 else null end) as sangat_kurang,
        COUNT(case when jd.jawaban = 'kurang' then 1 else null end) as kurang,
        COUNT(case when jd.jawaban = 'cukup' then 1 else null end) as cukup,
        COUNT(case when jd.jawaban = 'baik' then 1 else null end) as baik,
        COUNT(case when jd.jawaban = 'sangat_baik' then 1 else null end) as sangat_baik
        FROM
        jawaban as j 
        JOIN
        jawaban_detail as jd on j.id = jd.id_jawaban
        RIGHT JOIN
        satkers as s on s.id = j.satker_id
        where s.id = $id_satker
        GROUP BY s.nama_satker, s.id
        ");

        $nama_satker = DB::select("select nama_satker from satkers where id = $id_satker");

        return view('pages.admin.report.hasilsurveydetail',compact('query','query2','nama_satker'));
    }

    public function hasilSurveyPersonal($id_satker){


        $query = DB::select("
        select s.id as id, s.nama_satker as nama_satker, p.pertanyaan as pertanyaan,
        COUNT(case when jd.jawaban = 'sangat_kurang' then 1 else null end) as sangat_kurang,
        COUNT(case when jd.jawaban = 'kurang' then 1 else null end) as kurang,
        COUNT(case when jd.jawaban = 'cukup' then 1 else null end) as cukup,
        COUNT(case when jd.jawaban = 'baik' then 1 else null end) as baik,
        COUNT(case when jd.jawaban = 'sangat_baik' then 1 else null end) as sangat_baik
        from satkers s
        left join jawaban j on s.id = j.satker_id
        left join jawaban_detail jd on j.id = jd.id_jawaban
        left join pertanyaans p on jd.pertanyaan_id = p.id
        where s.id = $id_satker
        GROUP BY s.id, s.nama_satker, p.pertanyaan

        ");

        $query2 = DB::select("
        select s.nama_satker as nama_satker, s.id as id,
        COUNT(case when jd.jawaban = 'sangat_kurang' then 1 else null end) as sangat_kurang,
        COUNT(case when jd.jawaban = 'kurang' then 1 else null end) as kurang,
        COUNT(case when jd.jawaban = 'cukup' then 1 else null end) as cukup,
        COUNT(case when jd.jawaban = 'baik' then 1 else null end) as baik,
        COUNT(case when jd.jawaban = 'sangat_baik' then 1 else null end) as sangat_baik
        FROM
        jawaban as j 
        JOIN
        jawaban_detail as jd on j.id = jd.id_jawaban
        RIGHT JOIN
        satkers as s on s.id = j.satker_id
        where s.id = $id_satker
        GROUP BY s.nama_satker, s.id
        ");

        $nama_satker = DB::select("select nama_satker from satkers where id = $id_satker");

        return view('pages.admin.report.hasilsurveydetail',compact('query','query2','nama_satker'));
    }
}
