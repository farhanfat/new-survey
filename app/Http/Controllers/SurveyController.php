<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Jawaban;
use App\Models\JawabanDetail;
use App\Models\Satkers;

class SurveyController extends Controller
{
    public function surveyWithName($satker, $responden, $unique_kunjungan)
    {
        $data = DB::table('satkers')
            ->select("id", "nama_satker","slug")
            ->where('slug', '=', $satker);
        $dataSatker = $data->first();
        $data = array();
        $data['satker'] = $dataSatker;
        $data['nik_responden'] = $responden;
        $data['satker_url'] = $satker;

        $pertanyaan = DB::table('pertanyaans')
        ->select("id","pertanyaan")
        ->get();
        // ->orderBy("urutan","asc");
        // return view('frontoffice.indexWithName', $data);
        return view('pages.survey', compact('data','dataSatker','satker','pertanyaan', 'unique_kunjungan'));
    }

    public function saveAnswer(Request $request){
        $nikResponden = $request->nik_responden;
        $unique_kunjungan = $request->unique_kunjungan;
        $idSakter = $request->id_satker;
        $dataAnswer = $request->data_answer;
        $urlSatker = $request->url_satker;
        $pertanyaan = $request->pertanyaan;
        $jawaban = $request->jawaban;
        $nama_satkers = $request->nama_satker;
        
        
        $saveAnswer = new Jawaban;
        $saveAnswer->jawaban_dari = $nikResponden;
        $saveAnswer->satker_id = $idSakter;
        $result = $saveAnswer->save();
        $idAnswer = $saveAnswer->id;
        

        $setQuestion = array();
            foreach($pertanyaan as $key => $data) {
                
                $i = $key;          
                $saveDetailAnswer = new JawabanDetail;
                $saveDetailAnswer->id_jawaban = $idAnswer;
                $saveDetailAnswer->pertanyaan_id = $pertanyaan[$i];
                $saveDetailAnswer->jawaban = $jawaban[$i];
                $result = $saveDetailAnswer->save();

                $data = DB::table('pertanyaans')
                    ->select("pertanyaan")
                    ->where('id', '=', $pertanyaan[$i]);
                $dataQuestion = $data->first();

                $setQuestion[$key]['question'] = $dataQuestion->pertanyaan;
                if($jawaban[$i] == 'sangat_kurang'){
                    $setQuestion[$key]['answer'] = 'Sangat Kurang';
                }
                else if($jawaban[$i] == 'kurang'){
                    $setQuestion[$key]['answer'] = 'Kurang';
                }
                else if($jawaban[$i] == 'cukup'){
                    $setQuestion[$key]['answer'] = 'Cukup';
                }
                else if($jawaban[$i] == 'baik'){
                    $setQuestion[$key]['answer'] = 'Baik';
                }
                else if($jawaban[$i] == 'sangat_baik'){
                    $setQuestion[$key]['answer'] = 'Sangat Baik';
                }
                else {
                    $setQuestion[$key]['answer'] = '-';
                }
            }
            $token = env('KEJAKSAAN_TOKEN', '6400d8d96832a21178b875de6743eac7');
            $url = env('KEJAKSAAN_URL','https://bukutamu.kejaksaan.go.id/survey').'/'.$nama_satkers.'/'.$nikResponden;
                    
            $headers = array('Authorization: Bearer '.$token, 'Content-Type: application/json');

            $fields = array('payload' => $setQuestion);
                
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            $response = json_decode($result);
            $setResponse = array();
            // dd($response);
            // if(!empty($response->datas->unique_kunjungan)){
            $setUrl = env('KEJAKSAAN_URL_NIK','https://bukutamu.kejaksaan.go.id/kunjungan').'/'.$unique_kunjungan;
            $setValid = true;
            // $msg = $response->message;
            $msg = 'Data anda berhasil disimpan, terimakasih telah melakukan survey';
            // }else{
            //     $setUrl = env('APP_URL','http://localhost');
            //     $setValid = false;
            //     // $msg = $response->message;
            //     $msg = 'Terjadi Kesalahan';
            // }
            // $setResponse['response'] = $setValid;
            // $setResponse['url'] = $setUrl;
            // $setResponse['message'] = $msg;

            // echo json_encode($setResponse);
        
        
        // return redirect($setUrl);
        
        return view('pages.finish', compact('setUrl'));
        
    }

    public function surveySatker($satker)
    {
        $data = str_replace("-"," ",$satker);

        // ->orderBy("urutan","asc");
        // return view('frontoffice.indexWithName', $data);
        return view('pages.surveysatker', compact('data'));
    }
    
    
    public function index()
    {
        $datasatker = Satkers::all();
        return view('pages.indexsurvey', compact('datasatker'));
    }

    public function mulai(Request $request)
    {
        $namaSatker = $request->nama_satker;
        $nik = $request->nik;

        return redirect('/survey/'.$namaSatker.'/'.$nik);
    }

}
