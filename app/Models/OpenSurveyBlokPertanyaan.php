<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpenSurveyBlokPertanyaan extends Model
{
    use HasFactory;

    public $table = 'os_survey_blok_pertanyaan';


    protected $fillable = [
        'id',
        'id_survey_blok',
        'pertanyaan',
        'urutan',
    ];
}
