<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{

    public $table = 'jawaban';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'jawaban_dari',
        'satker_id',
        'updated_at',
        'created_at',
        'deleted_at',
    ];
}
