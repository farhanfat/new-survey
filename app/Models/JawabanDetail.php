<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JawabanDetail extends Model
{
    public $table = 'jawaban_detail';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'id_jawaban',
        'pertanyaan_id',
        'jawaban',
        'updated_at',
        'created_at',
        'deleted_at',
    ];
}
