<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LaporanHasilSurvey extends Model
{
    use HasFactory;

    public static function search($query, $dateStart, $dateEnd)
    {
        $data = DB::table('satkers as s')
        ->select(DB::raw('s.id, s.nama_satker as nama_satker,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'sangat_kurang\' GROUP BY jawaban_detail.jawaban) as sangat_kurang,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'kurang\' GROUP BY jawaban_detail.jawaban) as kurang,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'cukup\' GROUP BY jawaban_detail.jawaban) as cukup,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'baik\' GROUP BY jawaban_detail.jawaban) as baik,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'sangat_baik\' GROUP BY jawaban_detail.jawaban) as sangat_baik
        '))
        ->leftJoin('jawaban as j', 's.id', '=', 'j.satker_id')
        ->leftJoin('jawaban_detail as jd', 'j.id', '=', 'jd.id_jawaban');

        if(!empty($query)){
            $data->where('s.nama_satker', 'like', '%'.strtoupper($query).'%');
        }
        if(!empty($dateStart)){
            $dateStart = $dateStart." 00:00:00";
            $data->where('jd.created_at', '>=', $dateStart);
        }

        if(!empty($dateEnd)){
            $dateEnd = $dateEnd." 23:59:59";
            $data->where('jd.created_at', '<=', $dateEnd);
        }

        $data->groupBy('s.nama_satker');
        $data->groupBy('s.id');

        return $data;
    }
}
