<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpenSurvey extends Model
{

    public $table = 'os_survey';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'judul_survey',
        'deskripsi_survey',
        'survey_link',
        'periode_start',
        'periode_end',
        'is_open',
        'status',
        'updated_at',
        'created_at',
        'deleted_at',
    ];
}
