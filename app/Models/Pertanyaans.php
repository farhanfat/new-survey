<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaans extends Model
{
    use HasFactory;
    public $table = 'pertanyaans';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        //'id',
        'urutan',
        'pertanyaan',
        'updated_at',
        'created_at',
        'deleted_at',
    ];
}
