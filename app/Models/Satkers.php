<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Satkers extends Model
{

    public $table = 'satkers';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'parent_satker',
        'nama_satker',
        'tipe_satker',
        'updated_at',
        'created_at',
        'deleted_at',
    ];


    public function parent_satkers()

    {
        return $this->belongsTo('App\Models\Satkers','id','parent_satker');
    }
    
}
