<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpenSurveyBlok extends Model
{
    public $table = 'os_survey_blok';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id',
        'id_survey',
        'nama_blok',
        'deskripsi_blok',
        'slug_blok',
        'urutan',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
