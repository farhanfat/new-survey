<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpenSurveyBlokJawaban extends Model
{
    use HasFactory;

    public $table = 'os_survey_blok_jawaban';


    protected $fillable = [
        'id',
        'id_survey_blok_pertanyaan',
        'jawaban',
        'urutan',
    ];
}
