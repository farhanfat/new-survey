<?php

use App\Http\Controllers\Admin\OpensurveyController;
use App\Http\Controllers\Admin\SatkerController;
use App\Http\Controllers\Admin\PertanyaanController;
use App\Http\Controllers\Admin\PenggunaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login');
})->name('login');

// Route::get('/login', function () {
//     return view('pages.login');
// });

Route::get('/survey/{satker}/{name_responden}/{unique_kunjungan}', 'App\Http\Controllers\SurveyController@surveyWithName');
Route::get('/survey-satker/{satker}', 'App\Http\Controllers\SurveyController@surveySatker');
Route::get('/survey', 'App\Http\Controllers\SurveyController@index');
Route::post('/survey/simpan', 'App\Http\Controllers\SurveyController@saveAnswer');
Route::post('/survey/mulai', 'App\Http\Controllers\SurveyController@mulai');

Route::get('/admin', function () {
    return view('pages.login');
});

Route::get('/admin/register', function () {
    return view('pages.register');
});



// Route::get('/admin/pertanyaan', 'PertanyaanController@index');


// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', 'App\Http\Controllers\Admin\DashboardController@index' )->name('dashboard');

Route::group(['prefix' => 'admin','as'=>'admin.','middleware' =>['auth:sanctum', 'verified']],
function(){
    Route::resource('satker',SatkerController::class);

});

Route::group(['prefix' => 'admin','as'=>'admin.','middleware' =>['auth:sanctum', 'verified']],
function(){
    Route::resource('pertanyaan',PertanyaanController::class);

    Route::get('hasilsurvey', 'App\Http\Controllers\Admin\ReportController@hasilsurvey');
    Route::get('exporthasilsurvey', 'App\Http\Controllers\Admin\ReportController@exporthasilsurvey');
    Route::get('hasilsurveysatker', 'App\Http\Controllers\Admin\ReportController@hasilsurveysatker');
    Route::get('hasilsurveypersonal/{id_satker}', 'App\Http\Controllers\Admin\ReportController@hasilsurveypersonal');
    Route::post('hasilsurveysatkerdetail', 'App\Http\Controllers\Admin\ReportController@hasilsurveysatkerdetail')->name('hasilsurveysatkerdetail');
    Route::resource('opensurvey',OpensurveyController::class);
    Route::get('opensurvey/detail/{id}',[App\Http\Controllers\Admin\OpensurveyController::class, 'detail']);
    Route::put('opensurvey/detail/{id}',[App\Http\Controllers\Admin\OpensurveyController::class, 'ubahblok']);
    Route::post('opensurvey/detail',[App\Http\Controllers\Admin\OpensurveyController::class, 'simpanblok']);
    Route::post('opensurvey/pertanyaan',[App\Http\Controllers\Admin\OpensurveyController::class, 'simpanpertanyaan']);
    Route::delete('opensurvey/detail/{id}',[App\Http\Controllers\Admin\OpensurveyController::class, 'hapusblok']);
    Route::get('opensurvey/detail/table/{id_survey_blok}',[App\Http\Controllers\Admin\OpensurveyController::class, 'api_pertanyaan']);
    Route::get('opensurvey/detail/get/{id_survey_blok}',[App\Http\Controllers\Admin\OpensurveyController::class, 'get_pertanyaan']);
});

Route::group(['prefix' => 'admin','as'=>'admin.','middleware' =>['auth:sanctum', 'verified']],
function(){
    Route::resource('pengguna',PenggunaController::class);

});

// Route::get('/dashboard/pengguna', 'App\Http\Controllers\Admin\PenggunaController@index');
// Route::get('/dashboard/{name}/edit', 'App\Http\Controllers\Admin\PenggunaController@edit');
// Route::put('/dashboard/{name}', 'App\Http\Controllers\Admin\PenggunaController@update');
// Route::delete('/dashboard/{name}', 'App\Http\Controllers\Admin\PenggunaController@delete');