<!doctype html>
<html lang="en">
    <head>
        @include('includes.meta')
        <title>@yield('title')| E-SURVEY KEJAKSAAN</title>
        @stack('before-style')

        @include('includes.style')

        @stack('after-style')
    </head>

    <body>
        <div class="wrapper">
            @include('includes.sidebar')
            <div class="main-panel">
                @include('includes.topbar')
                @include('sweetalert::alert')
                @yield('content')
                @include('includes.footer')
            </div>
            
        </div>
        {{-- <div class="fixed-plugin">
            @include('includes.fixedplugin')
        </div> --}}
        
    </body>
    @stack('before-script')
    @include('includes.script')
    @stack('after-script')
</html>