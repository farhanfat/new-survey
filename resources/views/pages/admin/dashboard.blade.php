@extends('layouts.admin')

@section('title', ' Dashboard ')

@section('content')
@section('breadcrumb', ' Dashboard ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">person</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Total Admin</p>
                            @forelse ($total_user ?? '' as $key => $data)
                            <h3 class="card-title">{{ $data->total_user }}</h3>    
                            @empty
                                
                            @endforelse ( $total_user ?? '' as $data)
                            
                            
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="blue">
                            <i class="material-icons">list</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Total Survey</p>
                            @foreach ( $total_survey ?? '' as $total_survey)
                            <h3 class="card-title">{{ $total_survey->total_survey }}</h3>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">info</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Survey Hari Ini</p>
                            
                            <h3 class="card-title">{{ $total_survey_today}}</h3>    
                            
                            
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Top 10 Satker Survey Terbanyak</h4>
                        <div class="material-datatables">
                                        
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nama Satker</th>
                                        <th>Jumlah Survey</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($jumlah_survey as $key => $jumlah_survey )
                                    <tr> 
                                        <td>{{ $jumlah_survey->nama_satker }}</td>
                                        <td>{{ $jumlah_survey->jumlah }}</td>
                                    </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table> 
                        </div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>



@endsection


@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });

    function confirmationDelete() {
    confirm("Apakah anda yakin untuk menghapus")
    }
</script>
    
@endpush