@extends('layouts.admin')

@section('title', ' Pengguna ')

@section('content')
@section('breadcrumb', ' Tambah Pengguna ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <form action="{{ route('admin.pengguna.store') }}" method="POST">
                            @csrf
                            <div class="card-content">
                                <h4 class="card-title">Tambah Pengguna</h4>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Nama
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="name" required="true" />
                                    @error('name')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Username
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="username" required="true" />
                                    @error('username')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Email 
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="email" required="true" />
                                    @error('email')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Password
                                        <small>*</small>
                                    </label>
                                    <input type = "password" class="form-control" name="password" required="true" />
                                    @error('password')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Satker
                                        <small>*</small>
                                    </label>
                                    <select name="satker_id" id="satker_id" class="form-control">
                                        @forelse ($satker as $key => $satker )
                                        <option value="{{ $satker->id }}">{{ $satker->nama_satker }}</option>    
                                        @empty
                                            
                                        @endforelse
        
                                    </select> 
                                    @error('satker_id')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Roles
                                        <small>*</small>
                                    </label>
                                    <select name="roles" id="roles" class="form-control">
                                        <option value="Admin">== PILIH ROLES ==</option>
                                        <option value="Admin">Admin</option>
                                        <option value="satker">Satker</option>
                                    </select> 
                                    @error('roles')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                
                                <div class="category form-category">
                                    <small>*</small> Harus Diisi</div>
                                <div class="form-footer text-right">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="optionsCheckboxes"> Saya yakin menyimpan data ini
                                        </label>
                                    </div>
                                    <a href="{{ route('admin.pengguna.index') }}" class="btn btn-danger btn-fill">Kembali</a>
                                    <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });
</script>
    
@endpush