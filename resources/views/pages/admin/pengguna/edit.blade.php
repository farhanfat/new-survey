@extends('layouts.admin')

@section('title', ' Pengguna ')

@section('content')
@section('breadcrumb', ' Tambah Satker ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <form action="{{ route('admin.pengguna.update', $user->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="card-content">
                                <h4 class="card-title">Edit Pengguna</h4>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Nama Lengkap
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="name" required="true" value="{{ $user->name }}" />
                                    @error('name')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        username
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="username" required="true" value="{{ $user->username }}" />
                                    @error('username')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Email Pengguna
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="email" required="true" value="{{ $user->email }}" />
                                    @error('email')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Password
                                        <small>*</small>
                                    </label>
                                    <input type = "password" class="form-control" name="password"  value="" />
                                    <small>KOSONGKAN JIKA TIDAK INGIN UBAH PASSWORD</small>
                                    @error('password')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Satker
                                        <small>*</small>
                                    </label>
                                    <select name="satker_id" id="satker_id" class="form-control">
                                        
                                        <option value="{{ $user->id_satker }}">{{ $user->nama_satker }}</option>
                                        @foreach ($satker as $data)
                                        <option value="{{$data->id}}">{{ $data->nama_satker }}</option>    
                                        @endforeach
                                    </select> 
                                    @error('satker_id')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        ROLES
                                        <small>*</small>
                                    </label>
                                    <select name="roles" id="roles" class="form-control">
                                        <option value="{{ $user->roles }}">{{ $user->roles }}</option>
                                    </select> 
                                    @error('roles')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="category form-category">
                                    <small>*</small> Harus Diisi</div>
                                <div class="form-footer text-right">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="optionsCheckboxes"> Saya yakin mengubah data ini
                                        </label>
                                    </div>
                                    <a href="{{ route('admin.pengguna.index') }}" class="btn btn-danger btn-fill">Kembali</a>
                                    <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });
</script>
    
@endpush