@extends('layouts.admin')

@section('title', ' Pengguna Satker ')

@section('content')
@section('breadcrumb', ' Pengguna Satker')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Pengguna</h4>
                            <div class="material-datatables">
                            <a href="{{ route('admin.pengguna.create')}}"> <button class="btn btn-primary" style="float: right;">Tambah</button></a>
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Nama</th>
                                            <th>Username</th>
                                            <th>Email Pengguna</th>
                                            <th>Satker</th>
                                            <th>Role</th>
                                            <th width="15%" class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($user as $key => $user )
                                        <tr>
                                            
                                            <td>{{$key + 1}}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->nama_satker }}</td>
                                            <td>{{ $user->roles }}</td>
                                            <td class="td-actions text-right">
                                                <form action="{{ route('admin.pengguna.destroy', $user->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a href="{{ route('admin.pengguna.show', $user->id) }}" type="button" rel="tooltip" class="btn btn-info btn-simple" data-original-title="" title="">
                                                        <i class="material-icons">list</i>
                                                    </a>
                                                    <a href="{{ route('admin.pengguna.edit', $user->id) }}" type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <button onclick="confirmationDelete()" type="submit" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="" >
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </form>
                                            </td>
                                            {{-- <td class="td-actions text-right">
                                                <a href="{{ route('admin.pengguna.show', $user->id) }}" type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
                                                    <i class="material-icons">list</i>
                                                </a>
                                                <a href="{{ route('admin.pengguna.edit', $user->id) }}" type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
                                                    <i class="material-icons">edit</i>
                                                </a>
                                                <form action="{{ route('admin.pengguna.destroy', $user->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button onclick="confirmationDelete()" type="submit" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="" >
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </form>
                                            </td> --}}
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                    
                                </table>
                                
                                    
                                
                                
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });

    function confirmationDelete() {
    confirm("Apakah anda yakin untuk menghapus")
    }
</script>
    
@endpush