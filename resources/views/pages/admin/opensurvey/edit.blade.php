@extends('layouts.admin')

@section('title', ' Pengaturan Open Survey ')

@section('content')
@section('breadcrumb', ' Ubah Open Survey ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <form action="{{ route('admin.opensurvey.update', $data->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <input name="status" value="1" hidden/>
                            <div class="card-content">
                                <h4 class="card-title">Ubah Pengaturan Open Survey</h4>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Judul Survey
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="judul_survey" value="{{ $data->judul_survey }}" required="true" />
                                    @error('judul_survey')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Link Survey
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="survey_link" value="{{ $data->survey_link }}" required="true" />
                                    @error('survey_link')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror                      
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Deskripsi Survey
                                        <small>*</small>
                                    </label>
                                    <textarea class="form-control" name="deskripsi_survey" value="{{ $data->deskripsi_survey }}" required="true" >{{ $data->deskripsi_survey }}</textarea>
                                    @error('deskripsi_survey')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror                      
                                </div>
                                <div class="row">
                                    <div class="form-group label-floating col-md-6">
                                        <label class="label-control">
                                            Periode Start
                                            <small>*</small>
                                        </label>
                                        <input type="date" class="form-control datepicker" value="{{ $data->periode_start }}" name="periode_start" required="true" />
                                        @error('periode_start')
                                            <div class="alert alert-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror                      
                                    </div>
                                    <div class="form-group label-floating col-md-6">
                                        <label class="label-control">
                                            Periode End
                                            <small>*</small>
                                        </label>
                                        <input type="date" class="form-control datepicker" value="{{ $data->periode_end }}" name="periode_end" required="true" />
                                        @error('periode_end')
                                            <div class="alert alert-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror                      
                                    </div>
                                </div>
                                <div class="form-group label-floating checkbox-radios">
                                    <label class="label-control">
                                        Apakah Survey Untuk Publik?
                                        <small>*</small>
                                    </label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="1" @if($data->is_open =="1") checked="true" @endif name="is_open" > Ya, Survey untuk publik
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="2"  @if($data->is_open =="2") checked="true" @endif name="is_open" > Tidak, Survey hanya untuk internal kejaksaan
                                        </label>
                                    </div>
                                    @error('is_open')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror                      
                                </div>
                                <div class="form-group label-floating checkbox-radios">
                                    <label class="label-control">
                                        Status
                                        <small>*</small>
                                    </label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="1" @if($data->status =="1") checked="true" @endif name="status" > Active
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="2"  @if($data->status =="2") checked="true" @endif name="status" > Non Active
                                        </label>
                                    </div>
                                    @error('status')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror                      
                                </div>
                                <div class="category form-category">
                                    <small>*</small> Harus Diisi</div>
                                <div class="form-footer text-right">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="optionsCheckboxes"> Saya yakin menyimpan data ini
                                        </label>
                                    </div>
                                    <a href="{{ route('admin.opensurvey.index') }}" class="btn btn-danger btn-fill">Kembali</a>
                                    <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });
</script>
    
@endpush