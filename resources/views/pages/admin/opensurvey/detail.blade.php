@extends('layouts.admin')

@section('title', 'Pengaturan Open Survey ')

@section('content')
@section('breadcrumb', ' Pengaturan Open Survey ')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <p hidden>{{ $id_data = $data->id }}</p>
                    <p hidden>{{ $count_pertanyaan }}</p>
                    <div class="card-content">
                        <button class="btn btn-primary" style="float: right;" data-toggle="modal" data-target="#modalTambahBlok">Tambah Blok Survey</button>
                        <a href="#"> <button class="btn btn-secondary" style="float: right;">Kunjungi Link Survey</button></a>
                        <h4 class="card-title">Detail Open Survey {{ $data->judul_survey }}</h4>
                        <div class="">
                            <div class="card">
                                    <!--        You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                                <div class="">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td class="text-primary" width="300">Judul Survey</td>
                                                    <td>{{ $data->judul_survey }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Deskripsi Survey</td>
                                                    <td>{{ $data->deskripsi_survey }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Link Survey</td>
                                                    <td>{{ $data->survey_link }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Periode Start</td>
                                                    <td>{{ $data->periode_start }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Periode End</td>
                                                    <td>{{ $data->periode_end }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Metode Survey</td>
                                                    <td>@if($data->is_open == "1") Publik
                                                        @else Internal
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-primary" width="300">Status</td>
                                                    <td>@if($data->status == "1") Active
                                                        @else Non Active
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                            @forelse ($blok as $key => $blok )
                                            <li>
                                                <a id="tables{{ $key+1 }}" data-id="{{ $blok->id }}" href="#tabdata{{ $key+1 }}" data-toggle="tab">Blok {{ $key+1 }} ({{ $blok->nama_blok }})</a>
                                            </li>
                                            @empty
                                            <li>
                                                <a href="#" data-toggle="tab">Blok Kosong</a>
                                            </li> 
                                            @endforelse
                                        </ul>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="tab-content">
                                            @forelse ($bloks as $key => $bloks )
                                            <div class="tab-pane" id="tabdata{{ $key+1 }}">
                                                <hr/>
                                                <button class="btn btn-secondary" style="float: right;" data-toggle="modal" data-target="#edit{{ $bloks->slug_blok }}">Edit Blok Ini</button>
                                                <form action="{{ url('admin/opensurvey/detail/'.$bloks->id) }}" method="POST">
                                                    <input type="text" value="{{ $data->id }}" name="id_survey" hidden>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button onclick="confirmationDelete()" type="submit" rel="tooltip" class="btn btn-danger">
                                                        Hapus Blok Ini
                                                    </button>
                                                    <hr/>
                                                </form>  
                                                <center>
                                                    <h4>Daftar Pertanyaan {{ $bloks->nama_blok }}</h4>
                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalTambahPertanyaan">Tambah Pertanyaan</button>
                                                </center>
                                                <table class="table">
                                                    <thead class="text-primary">
                                                        <th>Pertanyaan</th>
                                                        <th>Aksi</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Siapa Ibumu</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Siapa Bapakmu</td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            @empty
                                            <div class="tab-pane" id="#">
                                                Blok Kosong Buat Blok Terlebih Dahulu
                                            </div>
                                            @endforelse
                                    </div>
                                            
                                </div>
                            </div>
                        </div>
                                    
                            
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<div class="modal fade" id="modalTambahBlok" tabindex="-1" aria-labelledby="modalTambahBlok" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Blok Survey {{ $data->judul_survey }}</h5>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('admin/opensurvey/detail') }}" method="POST">
                    @csrf
                    <input type="text" value="{{ $data->id }}" name="id_survey" hidden>
                    <input type="text" value="{{ $data->survey_link }}" name="survey_link" hidden>
                    <div class="form-group">
                        <label for="">Nama Blok</label>
                        <input type="text" class="form-control" id="nama_blok" name="nama_blok">
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi Blok</label>
                        <input type="text" class="form-control" id="deskripsi_blok" name="deskripsi_blok">
                    </div>
                    <div class="form-group">
                        <label for="">Urutan Blok</label>
                        <input type="text" class="form-control" id="urutan" name="urutan">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTambahPertanyaan" tabindex="-1" aria-labelledby="modalTambahPertanyaan" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pertanyaan</h5>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('admin/opensurvey/pertanyaan') }}" method="POST">
                    @csrf
                    <input name="id_survey_blok" value={{ $bloks->id }} }} hidden/>
                    <table class="table table-bordered" id="dynamicAddRemove">
                        <tr>
                            <th>Pertanyaan</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td><input type="text" name="pertanyaan[0]" placeholder="Masukkan Pertanyaan" class="form-control" />
                            </td>
                            <td><button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary">Tambah</button></td>
                        </tr>
                    </table>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </form>
            </div>
        </div>
    </div>
</div>

@forelse ($blokz as $key => $blokz )
    <div class="modal fade" id="edit{{ $blokz->slug_blok }}" tabindex="-1" aria-labelledby="modalEditBlok" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Blok {{ $blokz->nama_blok }}</h5>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('admin/opensurvey/detail/'.$blokz->id) }}" method="POST">
                        <input type="text" value="{{ $data->id }}" name="id_survey" hidden>
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">Nama Blok</label>
                            <input type="text" class="form-control" value="{{ $blokz->nama_blok }}" id="nama_blok" name="nama_blok">
                        </div>
                        <div class="form-group">
                            <label for="">Deskripsi Blok</label>
                            <input type="text" class="form-control" value="{{$blokz->deskripsi_blok }}"  id="deskripsi_blok" name="deskripsi_blok">
                        </div>
                        <div class="form-group">
                            <label for="">Urutan Blok</label>
                            <input type="text" class="form-control" value="{{ $blokz->urutan }}" id="urutan" name="urutan">
                        </div>
                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@empty
                                        
@endforelse 
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="pertanyaan[]" placeholder="Masukkan Pertanyaan" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });

    $(document).ready(function() {

        var count_pertanyaan = {{ $count_pertanyaan }};
        console.log(count_pertanyaan,'ini cuuuk')
        
        count_pertanyaan = {{ $count_pertanyaan }}
        
        

        for (let i= 1; i <= count_pertanyaan; i++){
             
             
            $('#tables'+i).on('click', function() {
            var data_blok = $(this).data('id');
            // const Url='get/' + data_blok;
            const xhttp = new XMLHttpRequest();
            xhttp.open("GET", "get/" + data_blok);
            xhttp.send();
            xhttp.onreadystatechange = function(response) {
        

            }
        })
        }

        function confirmationDelete() {
        confirm("Apakah anda yakin untuk menghapus")
        }
        

        
        
        
        

        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });

    function confirmationDelete() {
    confirm("Apakah anda yakin untuk menghapus")
    }
</script>
    
@endpush