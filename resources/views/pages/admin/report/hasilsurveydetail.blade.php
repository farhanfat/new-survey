@extends('layouts.admin')

@section('title', ' Hasil Survey ')

@section('content')
@section('breadcrumb', ' Hasil Survey ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            {{-- <a href="javascript:window.print()"> <button class="btn btn-primary" style="float: right;">PRINT</button></a> --}}
                            <h4 class="card-title">Laporan Hasil Survey Satker Detail</h4>
                            {{-- <h4 class="card-title">{{ $query2->nama_satker }}</h4> --}}
                            <br/>
                            @forelse ($nama_satker as $key => $nama_satker)
                            <center><h3 class="card-title">{{ $nama_satker->nama_satker }}</h3></center>
                            @empty
                            @endforelse
                            <div class="material-datatables">
                                
                                <table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    
                                    <thead>
                                        <center><h5 class="card-title">Jawaban Survey</h5></center>
                                        <tr>
                                            <th>Pertanyaan</th>
                                            <th>Sangat Buruk</th>
                                            <th>Buruk</th>
                                            <th>Cukup</th>
                                            <th>Baik</th>
                                            <th>Sangat Baik</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($query as $key => $query )
                                        <tr> 
                                            <td>{{ $query->pertanyaan }}</td>
                                            <td>{{ $query->sangat_kurang }}</td>
                                            <td>{{ $query->kurang }}</td>
                                            <td>{{ $query->cukup }}</td>
                                            <td>{{ $query->baik }}</td>
                                            <td>{{ $query->sangat_baik }}</td>
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                    
                                </table>
                            </div>
                            <div class="">
                                <table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <br/><br/><br/>
                                        <center><h5 class="card-title">Total Jawaban Survey</h5></center>
                                        <tr>
                                            <th>Sangat Buruk</th>
                                            <th>Buruk</th>
                                            <th>Cukup</th>
                                            <th>Baik</th>
                                            <th>Sangat Baik</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($query2 as $key => $query2 )
                                        <tr> 
                                            <td>{{ $query2->sangat_kurang }}</td>
                                            <td>{{ $query2->kurang }}</td>
                                            <td>{{ $query2->cukup }}</td>
                                            <td>{{ $query2->baik }}</td>
                                            <td>{{ $query2->sangat_baik }}</td>
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [-1],
                ["All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });

    function confirmationDelete() {
    confirm("Apakah anda yakin untuk menghapus")
    }
</script>
    
@endpush