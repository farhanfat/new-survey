@extends('layouts.admin')

@section('title', ' Hasil Survey ')

@section('content')
@section('breadcrumb', ' Hasil Survey ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Laporan Hasil Survey Per-Satker</h4>
                            <form action="{{ url('/admin/hasilsurveysatkerdetail') }}" method="POST"> 
                                @csrf
                                <div class="">
                                    <p>Pilih Satker yang ingin anda lihat laporannya</p>
                                    <select class="selectpicker" name="id_satker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7">
                                        <option disabled selected>Pilih Satker</option>
                                        @forelse ($data as $key => $data )
                                        <option value="{{ $data->id }}">{{ $data->nama_satker }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                                <div class="form-footer text-right">
                                    <div class="checkbox pull-left">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-fill">Generate Laporan</button>
                                </div>
                            </form>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [-1],
                ["All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });

    function confirmationDelete() {
    confirm("Apakah anda yakin untuk menghapus")
    }
</script>
    
@endpush