@extends('layouts.admin')

@section('title', ' Satker ')

@section('content')
@section('breadcrumb', ' Tambah Satker ')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <form action="{{ route('admin.satker.store') }}" method="POST">
                            @csrf
                            <div class="card-content">
                                <h4 class="card-title">Tambah Satker</h4>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Nama Satker
                                        <small>*</small>
                                    </label>
                                    <input class="form-control" name="nama_satker" required="true" />
                                    @error('nama_satker')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Parent Satker
                                        <small>*</small>
                                    </label>
                                    <select name="parent_satker" id="parent_satker" class="form-control"> 
                                        <option value="">== PILIH PARENT ==</option>    
                                        @foreach ($datasatker as $data )
                                        <option value="{{ $data->id }}">{{ $data->nama_satker }}</option>    
                                        @endforeach 
                                    </select>      
                                    @error('parent_satker')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror              
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Tipe Satker
                                        <small>*</small>
                                    </label>
                                    <select name="tipe_satker" id="tipe_satker" class="form-control">
                                        <option value="">== PILIH TIPE ==</option>
                                        <option value="1">PUSAT</option>
                                        <option value="2">PROVINSI</option>
                                        <option value="3">KAB/KOTA</option>
                                    </select> 
                                    @error('tipe_satker')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror                                 
                                </div>
                                <div class="category form-category">
                                    <small>*</small> Harus Diisi</div>
                                <div class="form-footer text-right">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="optionsCheckboxes"> Saya yakin menyimpan data ini
                                        </label>
                                    </div>
                                    <a href="{{ route('admin.satker.index') }}" class="btn btn-danger btn-fill">Kembali</a>
                                    <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
    

@endsection

@push('after-script')

<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#datatables').DataTable();

        // Edit record
        table.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });
</script>
    
@endpush