<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/assets/img/apple-icon.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>E-SURVEY KEJAKSAAN</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="keywords" content="E-SURVEY KEJAKSAAN">
    <meta name="description" content="E-SURVEY KEJAKSAAN">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="E-SURVEY KEJAKSAAN">
    <meta itemprop="description" content="E-SURVEY KEJAKSAAN">
    <meta itemprop="image" content="https://www.kejaksaan.go.id/images/logo.png">
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('/assets/css/material-dashboard.css') }}" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('/assets/css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="{{ asset('/assets/css/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('/assets/css/google-roboto-300-700.css') }}" rel="stylesheet" />
</head>

<body>
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="{{ asset('/assets/img/back.png') }}">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-md-offset-2 col-sm-offset-3">
                            <form method="POST" action="{{ url('survey/simpan')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="card card-login card-hidden">
                                    <div class="card-header text-center" data-background-color="rose">
                                        <h4 class="card-title">SURVEY KEPUASAN TAMU</h4>
                                        <h3 class="card-title">{{$dataSatker->nama_satker ?? ''}}</h3>
                                        <input type="hidden" class="form-control" name="nik_responden" value="{{ $data['nik_responden'] }}" />
                                        <input type="hidden" name="id_satker" value="{{$dataSatker->id}}">
                                        <input type="hidden" name="unique_kunjungan" value="{{$unique_kunjungan}}">
                                        <input type="hidden" name="nama_satker" value="{{$dataSatker->nama_satker}}">
                                    </div>
                                    <div class="card-content">
                                        
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    
                                                </span>
                                                <div class="form-group label-floating">
                                                    @foreach ($pertanyaan as $key => $pertanyaan)
                                                    <p hidden>{{ $i = $key }}</p>
                                                    <p class="form-control-static">{{ $pertanyaan->pertanyaan }}</p>
                                                    <input type="hidden" name="pertanyaan[{{ $i}}]" value="{{ $pertanyaan->id }}">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="jawaban[{{$i}}]" value="sangat_kurang" required> Sangat Kurang
                                                            @error('jawaban[{{$i}}')
                                                                <div class="alert alert-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror    
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="jawaban[{{$i}}]" value="kurang" required> Kurang
                                                            @error('jawaban[{{$i}}')
                                                                <div class="alert alert-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror    
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="jawaban[{{$i}}]" value="cukup" required> Cukup
                                                            @error('jawaban[{{$i}}')
                                                                <div class="alert alert-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror    
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="jawaban[{{$i}}]" value="baik" required> Baik
                                                            @error('jawaban[{{$i}}')
                                                                <div class="alert alert-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror    
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="jawaban[{{$i}}]" value="sangat_baik" required> Sangat Baik
                                                            @error('jawaban[{{$i}}')
                                                                <div class="alert alert-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror    
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        
                                       
                                       
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">SIMPAN SURVEY</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="https://www.instagram.com/the_a_team_kejaksaan_ri/">The A Team</a>, Kejaksaan RI
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('/assets/js/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/material.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('/assets/js/jquery.validate.min.js') }}"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('/assets/js/moment.min.js') }}"></script>
<!--  Charts Plugin -->
<script src="{{ asset('/assets/js/chartist.min.js') }}"></script>
<!--  Plugin for the Wizard -->
<script src="{{ asset('/assets/js/jquery.bootstrap-wizard.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('/assets/js/bootstrap-notify.js') }}"></script>
<!--   Sharrre Library    -->
<script src="{{ asset('/assets/js/jquery.sharrre.js') }}"></script>
<!-- DateTimePicker Plugin -->
<script src="{{ asset('/assets/js/bootstrap-datetimepicker.js') }}"></script>
<!-- Vector Map plugin -->
<script src="{{ asset('/assets/js/jquery-jvectormap.js') }}"></script>
<!-- Sliders Plugin -->
<script src="{{ asset('/assets/js/nouislider.min.js') }}"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="{{ asset('/assets/js/jquery.select-bootstrap.js') }}"></script>
<!--  DataTables.net Plugin    -->
<script src="{{ asset('/assets/js/jquery.datatables.js') }}"></script>
<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('/assets/js/sweetalert2.js') }}"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{ asset('/assets/js/jasny-bootstrap.min.js') }}"></script>
<!--  Full Calendar Plugin    -->
<script src="{{ asset('/assets/js/fullcalendar.min.js') }}"></script>
<!-- TagsInput Plugin -->
<script src="{{ asset('/assets/js/jquery.tagsinput.js') }}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('/assets/js/material-dashboard.js') }}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('/assets/js/demo.js') }}"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
    
    
</script>


</html>