<div class="sidebar" data-active-color="rose" data-background-color="black" data-image=".{{ asset('/assets/img/sidebar-1.jpg') }}">
    <p hidden>{{ $user = Auth::user(); }}<p>
    <div class="logo">
        
        <a href="http://www.creative-tim.com/" class="simple-text">
            E-SURVEY
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="http://www.creative-tim.com/" class="simple-text">
            E-S
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ asset('/assets/img/faces/avatar.jpg') }}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    {{ $user->name }}
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.pengguna.show', $user->id) }}">My Profile</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.pengguna.edit', $user->id) }}">Edit Profile</a>
                        </li>
                        
                        <li>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-jet-dropdown-link href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-jet-dropdown-link>
                            </form>
                        </li>

                        
                    </ul>
                </div>
            </div>
        </div>
        
        @if ($user->roles == 'satker')
        <ul class="nav">
            <li class="">

                <a href="{{route('dashboard')}}">
                <a href="/dashboard">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#survey">
                    <i class="material-icons">report</i>
                    <p>Buku Tamu
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="survey">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('/admin/hasilsurveypersonal', $user->satker_id) }}">Hasil Survey </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>  
        @else
        <ul class="nav">
            <li class="">
                <a href="/dashboard">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#survey">
                    <i class="material-icons">book</i>
                    <p>Buku Tamu
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="survey">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('/admin/hasilsurvey') }}">Hasil Survey Keseluruhan</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/hasilsurveysatker') }}">Hasil Survey Per-Satker</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#opensurvey">
                    <i class="material-icons">public</i>
                    <p>Open Survey
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="opensurvey">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('/admin/opensurvey') }}">Pengaturan Open Survey</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#referensi">
                    <i class="material-icons">apps</i>
                    <p>Referensi
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="referensi">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.pertanyaan.index') }}">Pertanyaan</a>
                        </li>
                    </ul>
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.satker.index') }}">Satker</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#pengaturan">
                    <i class="material-icons">settings</i>
                    <p>Pengaturan 
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="pengaturan">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.pengguna.index') }}">Pengguna</a>
                        </li>
                    </ul>
                </div>
            </li>

        </ul>
        @endif
    </div>
</div>