<!-- Bootstrap core CSS     -->
<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="{{ asset('/assets/css/material-dashboard.css') }}" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="{{ asset('/assets/css/demo.css') }}" rel="stylesheet" />
<!--     Fonts and icons     -->
<link href="{{ asset('/assets/css/font-awesome.css') }}" rel="stylesheet" />
<link href="{{ asset('/assets/css/google-roboto-300-700.css') }}" rel="stylesheet" />