<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            {{-- <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul> --}}
        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="https://www.instagram.com/the_a_team_kejaksaan_ri/">The A Team</a>, Kejaksaan RI
        </p>
    </div>
</footer>