<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="{{ url('https://www.kejaksaan.go.id/images/logo-kejak.png') }}" />
<link rel="icon" type="image/png" href="{{ url('https://www.kejaksaan.go.id/images/logo-kejak.png') }}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<meta name="keywords" content="E-SURVEY KEJAKSAAN">
<meta name="description" content="E-SURVEY KEJAKSAAN">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="E-SURVEY KEJAKSAAN">
<meta itemprop="description" content="E-SURVEY KEJAKSAAN">
<meta itemprop="image" content="../../../s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">